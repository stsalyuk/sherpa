//
//  AppSettings.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 3/24/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


internal class AppSettings {
    
    internal static let shared = AppSettings()
    
    
    private init() {
        
    }
    
    
    internal func getSetting(_ key: String) -> AnyObject? {
        return UserDefaults.standard.object(forKey: key) as AnyObject?
    }
    
    internal func saveSetting(_ key: String, value: AnyObject?) {
        
        let userDefaults = UserDefaults.standard
        userDefaults.setValue(value, forKey: key)
        userDefaults.synchronize()
    }
    
    // The capacity of the advertisement cache.
    internal var cacheCapacity: Int {
        
        get {
            
            if let capacity = getSetting("cacheCapacity") as? Int {
                return capacity
            }
            
            return 48
        }
    }
    
    // The number of advertisements to use when calculating average distance and best fit line slope.
    internal var filterWindowSize: Int {
        
        get {
            if let size = getSetting("filterWindowSize") as? Int {
                return size
            }
            
            return 4
        }
    }
    
    // The radius (in meters) of a beacon when we are considered 'inside' its domain.
    internal var domainRadius: Double {
        
        get {
            if let radius = getSetting("domainRadius") as? Double {
                return radius
            }
            
            return 5.0
        }
    }
    
    
    internal var pinkBeaconNickname: String {
        
        get {
            if let nickname = getSetting("pinkBeaconNickname") as? String {
                return nickname
            }
            
            return "Pink"
        }
    }
    
    
    internal var blueBeaconNickname: String {
        get {
            if let nickname = getSetting("blueBeaconNickname") as? String {
                return nickname
            }
            
            return "Blue"
        }
    }
}
