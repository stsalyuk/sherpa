//
//  DeviceInfoViewModel.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import Charts


protocol DeviceUpdatedDelegate {
    
    
    func onUpdated()
    
    func onClear()
    
    func onConnected()
    
    func enteredDomain(_ nickname: String)
    
    func exitedDomain()
}


internal class DeviceInfoViewModel: BaseViewModel, DataChangedCallback {
    
    internal var delegate: DeviceUpdatedDelegate?
    
    internal var mode: ScanMode = .scanOnly
    
    private lazy var deviceManager = BluetoothManager.shared
    private lazy var dataManager = DataManager.shared
    
    internal var currentDomainName = ""
    internal var previousDomainName = ""
    
    internal var chartDataSet: LineChartDataSet!
    
    
    
    override init() {
    
        super.init()
        
        DataManager.shared.addListener(listener: self)
    }
    
    func updateChartData(_ lastAdvertisement: AdvertisementInfo) {
    
        let lastTenValues = dataManager.advertisementHistory.getLast(count: 100, byId: lastAdvertisement.beaconId)
        
        var yValues: [Double] = []
        
        for val in lastTenValues {
            
            if !val.isOutlier {
                yValues.append(val.distance)
            }
        }
        
        var entries: [ChartDataEntry] = Array()
        
        for (i, value) in yValues.enumerated() {
            
            entries.append(ChartDataEntry(x: Double(i), y: value))
        }
        
        chartDataSet = LineChartDataSet(values: entries, label: BeaconRegistrar.getNickname(lastAdvertisement.beaconId))
        let color = BeaconRegistrar.getChartColor(lastAdvertisement.beaconId)
        chartDataSet.setColor(color)
        chartDataSet.setCircleColor(color)
        chartDataSet.circleRadius = 2.0
        chartDataSet.circleHoleRadius = 1.0
    }
    
    func dataChanged() {
        
        if let advertisement = dataManager.lastAdvertisement {
            
            let avgDistance = dataManager.advertisementHistory.getAverageDistance(advertisement.beaconId)
            
            if avgDistance < AppSettings.shared.domainRadius {
            
                var beaconNickname = advertisement.beaconId
                if let beaconName = BeaconRegistrar.beaconMap[advertisement.beaconId] {
                    beaconNickname = beaconName
                }
                
                // Check that we aren't already in another domain
                let discoveredBeacons = dataManager.advertisementHistory.discoveredBeaconList
                
                var isClosestBeacon = true
                
                // Cycle through all the discovered beacons and confirm that we are closest to the beacon
                // in current advertisement.
                // NOTE: this assumes that distance to discovered beacons updates linearly.
                for beaconId in discoveredBeacons {
                    
                    if beaconId != advertisement.beaconId && dataManager.advertisementHistory.existsInCache(beaconId) {
                        
                        let distanceToNeighborBeacon = dataManager.advertisementHistory.getAverageDistance(beaconId)
                        
                        let currentBeaconData = dataManager.advertisementHistory.getLast(count: AppSettings.shared.filterWindowSize, byId: advertisement.beaconId)
                        let slopeToCurrentBeacon = AdvertisementHistory.getBestFitLineSlope(currentBeaconData)
                        
                        let neighborBeaconData = dataManager.advertisementHistory.getLast(count: AppSettings.shared.filterWindowSize, byId: beaconId)
                        let slopeToNeighborBeacon = AdvertisementHistory.getBestFitLineSlope(neighborBeaconData)
                        
                        if distanceToNeighborBeacon < avgDistance || (slopeToCurrentBeacon < -1.0 && slopeToNeighborBeacon > 1.0) {
                            isClosestBeacon = false
                            break
                        }
                    }
                }
                
                if isClosestBeacon {
                    
                    updateChartData(advertisement)
                    
                    delegate?.enteredDomain(beaconNickname)
                    
                    if currentDomainName != beaconNickname {
                    
                        previousDomainName = currentDomainName
                        currentDomainName = beaconNickname
                        
                        SpeechManager.shared.speak("Entered " + beaconNickname)
                    }
                }
            }
            
            if dataManager.numAdsReceived % 50 == 0 {
                print("\(Date()) recv: \(dataManager.numAdsReceived) advertisements")
            }
        }
        
        delegate?.onUpdated()
    }
    
}
