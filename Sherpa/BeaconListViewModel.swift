//
//  BeaconListViewModel.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/18/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import Charts


internal class BeaconListViewModel: BaseViewModel, DataChangedCallback {
    
    
    //internal var beaconList: [DeviceInfo] = []
    internal var beacons: [BeaconCellViewModel] = []
    
    private lazy var dataManager = DataManager.shared
    private lazy var batteryMonitor = BatteryMonitor.shared
    
    
    
    override init() {
        
        super.init()
        
        DataManager.shared.addListener(listener: self)
        BatteryMonitor.shared.addListener(listener: self)
    }
    
    func dataChanged() {
        
        // Update the beaconList using last recv'd advertisement
        if let advertisement = dataManager.lastAdvertisement {
            
            let nickname = BeaconRegistrar.getNickname(advertisement.beaconId)
            
            if let beacon = beacons.filter({ $0.nickname == nickname}).first {
                
                // already in list, update distance
               beacon.distance = advertisement.distance
            }
            else {
                
                //let beacon = DeviceInfo()
                let beacon = BeaconCellViewModel()
                beacon.distance = advertisement.distance
                beacon.nickname = nickname
                //beacon.connectionTime = advertisement.timestamp
                
                //beaconList.append(beacon)
                beacons.append(beacon)
            }
        }
        
        // Update the battery levels
        //for beacon in beaconList {
        for beacon in beacons {
            
            if let batteryData = batteryMonitor.batteryData.filter({ beacon.nickname == BeaconRegistrar.getNickname($0.beaconId) }).first {
                
                beacon.batteryLevel = batteryData.batteryLevel
            }
        }
        
        // Sort by distance such that closest beacon is on top of list.
        //beaconList.sort(by: { $0.distance < $1.distance })
        beacons.sort(by: { $0.distance < $1.distance })
    }
    
    
}
