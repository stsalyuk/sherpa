//
//  ViewController.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 12/30/16.
//  Copyright © 2016 Serge Tsalyuk. All rights reserved.
//

import UIKit


class SettingsViewController: UIViewController {
    
    @IBOutlet weak var cacheSlider: UISlider!
    @IBOutlet weak var filterSlider: UISlider!
    @IBOutlet weak var domainRadiusSlider: UISlider!
    
    @IBOutlet weak var blueBeaconTextField: UITextField!
    @IBOutlet weak var pinkBeaconTextField: UITextField!
    
    
    
    @IBAction func cacheSizeChanged(_ sender: Any) {
        
        let value = Int(cacheSlider.value)
        AppSettings.shared.saveSetting("cacheCapacity", value: value as AnyObject?)
    }
    
    
    @IBAction func filterWindowChanged(_ sender: Any) {
        
        let value = Int(filterSlider.value)
        AppSettings.shared.saveSetting("filterWindowSize", value: value as AnyObject?)
    }
    
    
    @IBAction func domainRadiusChanged(_ sender: Any) {
        
        AppSettings.shared.saveSetting("domainRadius", value: domainRadiusSlider.value as AnyObject?)
    }
    
    
    @IBAction func dismiss(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func pinkBeaconNameSet(_ sender: Any) {
        
        if let text = pinkBeaconTextField.text, text != "" {
        
            AppSettings.shared.saveSetting("pinkBeaconNickname", value: text as AnyObject?)
        }
    }
    
    
    @IBAction func blueBeaconNameSet(_ sender: Any) {
        
        if let text = blueBeaconTextField.text, text != "" {
            
            AppSettings.shared.saveSetting("blueBeaconNickname", value: text as AnyObject?)
        }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        cacheSlider.value = Float(AppSettings.shared.cacheCapacity)
        filterSlider.value = Float(AppSettings.shared.filterWindowSize)
        domainRadiusSlider.value = Float(AppSettings.shared.domainRadius)
        
        blueBeaconTextField.text = AppSettings.shared.blueBeaconNickname
        pinkBeaconTextField.text = AppSettings.shared.pinkBeaconNickname
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        
    }
}

