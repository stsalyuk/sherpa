//
//  BluetoothManager.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/3/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol ConnectionCallback {
    
    func peripheralConnected(_ peripheralManager: PeripheralManager)
    
    func peripheralDisconnected(_ peripheralManager: PeripheralManager)
    
    func peripheralTimedOut(_ peripheralManager: PeripheralManager)
}


internal class BluetoothManager : NSObject, CBCentralManagerDelegate {
    
    
    internal static let shared = BluetoothManager()
    
    private var centralManager: CBCentralManager!
    
    internal var isScanning = false
    
    private var peripheralManager: PeripheralManager?
    
    internal var callback: ConnectionCallback?
    
    private var connectionTimeout = 60.0
    
    
    private override init() {
        
        super.init()
        
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    
    internal func connect(_ peripheral: PeripheralManager) {
        
        if peripheralManager == nil {
            
            peripheralManager = peripheral
            centralManager.connect(peripheral.peripheral)
            
            Timer.scheduledTimer(timeInterval: connectionTimeout, target: self, selector: #selector(self.timeout), userInfo: nil, repeats: false)
        }
    }
    
    /**
     Time out after 60 seconds.
    */
    internal func timeout() {
        
        if let peripheral = peripheralManager {
            
            peripheral.didTimeoutConnecting = true
            
            disconnect(peripheral)
            
            callback?.peripheralTimedOut(peripheral)
        }
    }
    
    internal func disconnect(_ peripheralManager: PeripheralManager) {
        
        centralManager.cancelPeripheralConnection(peripheralManager.peripheral!)
    }
    
    internal dynamic func restartScanning() {
            
        stopScanning()
        startScanning()
    }
    
    // Callback method indicating that the CBCentralManager's state has changed.
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        if central.state == .poweredOn {
            
            startScanning()
            
            Timer.scheduledTimer(timeInterval: 30.0, target: self, selector: #selector(BluetoothManager.restartScanning), userInfo: nil, repeats: true)
        }
    }
    
    
    // Callback method indicating that a peripheral was discovered during a scan.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        if let name = peripheral.name {
            
            if name == "Tile" || name == "Allile_VI16" {
            
//                print("\nadvertisement:")
//                for (key, val) in advertisementData {
//                    print("[\(key)]=\(val)")
//                }
                //print("found \(name)")
                
                var manufacturerData = ""
                var powerLevel = 0.0
                
                if let data = advertisementData["kCBAdvDataManufacturerData"] as? Data {
                    manufacturerData = data.toByteArrayString()
                }
                
                if let data = advertisementData["kCBAdvDataTxPowerLevel"] as? Double {
                    powerLevel = data
                }
                
                guard powerLevel != 0.0 && manufacturerData != "" else {
                    return
                }
                
                let advertisement = AdvertisementInfo(name, data: manufacturerData, power: powerLevel, rssi: RSSI.doubleValue, peripheral: peripheral)
                advertisement.timestamp = Date()
                advertisement.distance = Utilities.calculateDistance(advertisement.rssi, txPower: advertisement.txPower)
                DataManager.shared.newAdvertisement(advertisement)
            }
        }
    }
    
    // Callback method indicating that a peripheral failed to connect.
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to connect")
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        if let currentPeripheral = peripheralManager, currentPeripheral.peripheral == peripheral {
            
            callback?.peripheralConnected(currentPeripheral)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        if let currentPeripheral = peripheralManager, currentPeripheral.peripheral == peripheral {
            
            callback?.peripheralDisconnected(currentPeripheral)
            peripheralManager = nil
        }
    }
    
    func startScanning() {

        //0x9d410007-35d6-f4dd-ba60-e7bd8dc491c0
        //tile uuid = 69f20aa1-a7ea-40df-bb26-8b56663999dc
        
        if !isScanning {
            
            //let uuids = [CBUUID(string: "FEEC"), CBUUID(string: "Allile_VI16")]
            let scanOptions = [CBCentralManagerScanOptionAllowDuplicatesKey : true]
            
            print("\nScanning - STARTED\n")
            centralManager.scanForPeripherals(withServices: [], options: scanOptions)
            isScanning = true
        }
        
    }
    
    func stopScanning() {
        
        if isScanning {
            
            print("\n\nScanning - STOPPED\n\n")
            centralManager.stopScan()
            isScanning = false
        }
    }
    
}
