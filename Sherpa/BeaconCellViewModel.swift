//
//  BeaconCellViewModel.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/25/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import Charts


internal class BeaconCellViewModel: BaseViewModel, DataChangedCallback {
    
    internal var chartDataSet: LineChartDataSet!
    
    private lazy var dataManager = DataManager.shared
    
    internal var nickname: String = ""
    internal var distance = 0.0
    internal var batteryLevel = 0
    
    override init() {
        
        super.init()
        
        DataManager.shared.addListener(listener: self)
    }
    
    func dataChanged() {
        
        if let advertisement = dataManager.lastAdvertisement {
        
            let beaconName = BeaconRegistrar.getNickname(advertisement.beaconId)
            
            if nickname == beaconName {
            
                updateChartData(advertisement)
                callback?.viewModelUpdated()
            }
        }
    }
    
    func updateChartData(_ lastAdvertisement: AdvertisementInfo) {
        
        let lastTenValues = dataManager.advertisementHistory.getLast(count: 100, byId: lastAdvertisement.beaconId)
        
        var yValues: [Double] = []
        
        for val in lastTenValues {
            
            if !val.isOutlier {
                yValues.append(val.distance)
            }
        }
        
        var entries: [ChartDataEntry] = Array()
        
        for (i, value) in yValues.enumerated() {
            
            entries.append(ChartDataEntry(x: Double(i), y: value))
        }
        
        chartDataSet = LineChartDataSet(values: entries, label: BeaconRegistrar.getNickname(lastAdvertisement.beaconId))
        let color = BeaconRegistrar.getChartColor(lastAdvertisement.beaconId)
        chartDataSet.setColor(color)
        chartDataSet.setCircleColor(color)
        chartDataSet.circleRadius = 2.0
        chartDataSet.circleHoleRadius = 1.0
    }
    
}
