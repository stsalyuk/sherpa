//
//  DeviceInfo.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


internal class DeviceInfo {
    
    internal var batteryLevel = -1
    internal var manufacturerName = ""
    internal var modelNumber = ""
    internal var txPower = 0.0
    
    internal var connectionTime = Date()
    
    internal var nickname: String = "Beacon"
    
    internal var distance: Double = 0.0
    
    
    init() {
        
    }
    
}
