//
//  Common.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/28/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import UIKit

public enum ScanMode {
    
    case scanOnly
    case connect
}


internal class BeaconRegistrar {
    
    
//    internal static var beaconMap: [String: String] = [
//        "a0d1ca" : "Blue",
//        "338af8" : "Pink"
//    ]
    
    
    internal static var beaconMap: [String: String] = [
        "a0d1ca" : AppSettings.shared.blueBeaconNickname,
        "338af8" : AppSettings.shared.pinkBeaconNickname
    ]
    
    private static var colorMap: [String: UIColor] = [
        "a0d1ca" : UIColor(red: 140.0/255.0, green: 234.0/255.0, blue: 255.0/255.0, alpha: 1.0),
        "338af8" : UIColor(red: 244.0/255.0, green: 66.0/255.0, blue: 131.0/255.0, alpha: 1.0)
    ]
    
    
    internal static func getNickname(_ id: String) -> String {
        
        var nickname = id
        
        if let name = beaconMap[id] {
            nickname = name
        }
        
        return nickname
    }
    
    
    internal static func getChartColor(_ id: String) -> UIColor {
        
        if let color = colorMap[id] {
            return color
        }
        
        return UIColor.blue
    }
}
