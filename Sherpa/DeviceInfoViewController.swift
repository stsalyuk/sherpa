//
//  AllileViewController.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import UIKit

import Charts

internal class DeviceInfoViewController: UIViewController, DeviceUpdatedDelegate {
    
    @IBOutlet weak var bleImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var deviceNameLabel: UILabel!
    @IBOutlet weak var manufacturerNameLabel: UILabel!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var manufacturerDataLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var batteryLabel: UILabel!
    @IBOutlet weak var RSSILabel: UILabel!
    @IBOutlet weak var txPowerLabel: UILabel!
    
    @IBOutlet weak var modeSwitch: UISegmentedControl!
    
    @IBOutlet weak var beaconAlertLabel: UILabel!
    
    @IBOutlet weak var chartView: UIView!
    
    private var iconView: SpinnerView?
    private var viewModel = DeviceInfoViewModel()
    
    var chart: LineChartView!
    var dataSet: LineChartDataSet!
    
    private var mode: ScanMode {
        get {
            return (modeSwitch.selectedSegmentIndex == 0) ? .scanOnly : .connect
        }
    }
    
    internal override func viewDidLoad() {
        
        super.viewDidLoad()
        
        resetFields()
        
        
        chart = LineChartView(frame: CGRect(x: 0, y: 0, width: chartView.frame.width, height: chartView.frame.height))
        //chart = LineChartView(frame: chartView.frame)
        chart.backgroundColor = NSUIColor.clear
        chart.leftAxis.axisMinimum = 0.0
        chart.rightAxis.axisMinimum = 0.0
        //chart.data = LineChartData(dataSet: dataSet)
        
        chartView.addSubview(chart)
    }
    
    internal override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.delegate = self
        self.addSpinner()
        animateSpinner()
    }
    
    @IBAction func modeChanged(_ sender: Any) {
        
        viewModel.mode = self.mode
    }
    
    internal func resetFields() {
        
        deviceNameLabel.text = "-------"
        manufacturerDataLabel.text = "------------"
        modelLabel.text = "-------"
        manufacturerNameLabel.text = "--------"
        batteryLabel.text = "--"
        RSSILabel.text = "--"
        txPowerLabel.text = "--"
        beaconAlertLabel.text = ""
        beaconAlertLabel.isHidden = true
    }
    
    
    internal func loadData() {
        
        let dataManager = DataManager.shared
        
        if let advertisement = dataManager.lastAdvertisement {
            
            
            
            deviceNameLabel.text = viewModel.currentDomainName
            
            manufacturerDataLabel.text = advertisement.manufacturerData
            RSSILabel.text = String(advertisement.rssi) + " dBm"
            txPowerLabel.text = String(dataManager.numAdsReceived) + " total ads recv'd"
            let distance = Utilities.calculateDistance(advertisement.rssi, txPower: advertisement.txPower)
            distanceLabel.text = String(distance) + "m"
            
            chart.data = LineChartData(dataSet: viewModel.chartDataSet)
            
        }
        else {
            resetFields()
        }
    }
    
    private func addSpinner() {
        
        let diameter = self.view.frame.width * (95/640)
        let spinnerFrame = CGRect(x: self.view.frame.width * 0.1, y: view.frame.height * 0.1, width: diameter, height: diameter)
        iconView = SpinnerView(frame: spinnerFrame)
        
        view.addSubview(iconView!)
    }
    
    
    internal func animateSpinner() {
        
        iconView!.mode = .spinner
        bleImage.isHidden = false
        statusLabel.text = "Searching..."
        statusLabel.layer.opacity = 0.3
        
        
        UIView.animate(withDuration: 2.0, delay: 0.0,
                       options: [UIViewAnimationOptions.repeat, .curveEaseInOut, .autoreverse],
                       animations: {() -> Void in
                        
                        self.statusLabel.layer.opacity = 1.0
        },completion: nil)
    }
    
    internal func animateCheckmark() {
        
        bleImage.isHidden = true
        statusLabel.text = "Connected"
        statusLabel.layer.opacity = 1.0
        statusLabel.layer.removeAllAnimations()
        
        iconView!.mode = .checkmark
    }
}



/**
 Implementation of DeviceUpdatedDelegate
*/
extension DeviceInfoViewController {
    
    internal func onUpdated() {
        
        loadData()
    }
    
    internal func onClear() {
        
        resetFields()
        animateSpinner()
    }
    
    internal func onConnected() {
        
        loadData()
        animateCheckmark()
    }
    
    func enteredDomain(_ nickname: String) {
        
        beaconAlertLabel.text = "Entered " + nickname + " domain"
        beaconAlertLabel.isHidden = false
    }
    
    func exitedDomain() {
        
        beaconAlertLabel.text = ""
        beaconAlertLabel.isHidden = true
    }
}


