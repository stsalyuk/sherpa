//
//  Data+Extension.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation

protocol DataConvertible {
    init?(data: Data)
    var data: Data { get }
}

extension DataConvertible {
    
    init?(data: Data) {
        guard data.count == MemoryLayout<Self>.size else { return nil }
        self = data.withUnsafeBytes { $0.pointee }
    }
    
    var data: Data {
        var value = self
        return Data(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
}

extension Int : DataConvertible { }

extension Float : DataConvertible { }

extension Double : DataConvertible { }


extension String: DataConvertible {
    
    init?(data: Data) {
        self.init(data: data, encoding: .utf8)
    }
    var data: Data {
        // Note: a conversion to UTF-8 cannot fail.
        return self.data(using: .utf8)!
    }
}

extension Data {
    
    func toByteArray() -> [UInt8] {
        
        var byteArray: [UInt8] = []
        
        self.withUnsafeBytes {(bytes: UnsafePointer<UInt8>)->Void in
            
            byteArray = Array(UnsafeBufferPointer(start: UnsafePointer<UInt8>(bytes), count: self.count))
        }
        
        return byteArray
    }
    
    func toByteArray16() -> [UInt16] {
        
        var byteArray: [UInt16] = []
        
        self.withUnsafeBytes {(bytes: UnsafePointer<UInt16>)->Void in
            
            let remainder: Int = count % 2
            
            byteArray = Array(UnsafeBufferPointer(start: UnsafePointer<UInt16>(bytes), count: Int(self.count / 2) + remainder ))
        }
        
        return byteArray
    }
    
    func toByteArrayString() -> String {
        
        let byteArray = toByteArray()
        var byteString = ""
        
        for bit in byteArray {
            //var st = String(format:"%2X", n)
            
            byteString += String(bit, radix: 16, uppercase: false)
        }
        
        return byteString
    }
    
    func toByteArrayString16() -> String {
    
        let byteArray = toByteArray16()
        var byteString = ""
        
        for bit in byteArray {
            byteString += String(bit)
        }
        
        return byteString
    }
    
}

