//
//  Utilities.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 3/23/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


internal class Utilities {
    
    internal static let maxDistanceInMeters = 11.0
    
    
    /**
     d = 10 ^ ((P-Rssi) / 10n) (n ranges from 2 to 4)
     */
    internal static func calculateDistance(_ rssi: Double, txPower: Double) -> Double {
        
        guard txPower != 0.0, rssi != 0.0 else {
            return 0
        }
        
        let n = 2.0     //exponent typically ranges between 2 - 2.5
        let dist = pow(10, (txPower - rssi) / (10.0 * n))
        return dist
    }
    
    
    /*
     
     square root of [ (1/N) times Sigma i=1 to N of (xi - mu)^2 ]
     
    */
    internal static func computeStandardDeviation(_ values: [Double]) -> Double {
        
        var sigma = 0.0
        
        guard values.count > 0 else {
            return 0.0
        }
        
        let mean = values.reduce(0, +) / Double(values.count)
        
        var squaredDiffs: [Double] = []
        
        for i in values {
            
            let squaredDiff = pow((i - mean), 2)
            squaredDiffs.append(squaredDiff)
        }
        
        let meanSquaredDiffs = squaredDiffs.reduce(0, +) / Double(squaredDiffs.count)
        
        sigma = sqrt(meanSquaredDiffs)
        
        return sigma
    }
    
    internal static func getBestFitLineSlope(xValues: [Double], yValues: [Double]) -> Double {
        
        if xValues.count != yValues.count {
            return 0
        }
        
        let meanX = xValues.reduce(0, +) / Double(xValues.count)
        let meanY = yValues.reduce(0, +) / Double(yValues.count)
        
        var top = 0.0
        var bottom = 0.0
        
        for i in 0..<xValues.count {
            
            top += ((xValues[i] - meanX) * (yValues[i] - meanY))
            bottom += ((xValues[i] - meanX) * (xValues[i] - meanX))
        }
        
        let slope = top / bottom
        
        return slope
    }
}
