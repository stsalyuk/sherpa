//
//  BeaconListViewController.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 2/1/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import UIKit

internal class BeaconListViewController : UITableViewController, DataChangedCallback, ViewModelCallback {
    
    
    fileprivate var viewModel = BeaconListViewModel()
    
    internal override func viewDidLoad() {
        
        super.viewDidLoad()
        
        DataManager.shared.addListener(listener: self)
    }
    
    func viewModelUpdated() {
        
        for i in 0..<tableView.numberOfRows(inSection: 0) {
            
            if let cell = tableView.cellForRow(at: IndexPath(row: i, section: 0)) as? BeaconListCell {
                
                cell.reloadChart()
                
                cell.batteryLevelLabel.text = String(cell.viewModel.batteryLevel) + " %"
                cell.beaconName.text = cell.viewModel.nickname
                cell.distanceLabel.text = String(format: "%.2f", cell.viewModel.distance) + " m"
            }
        }
    }
    
    func dataChanged() {
        
        tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.beacons.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeaconListCell") as? BeaconListCell
        let beaconCellViewModel = viewModel.beacons[indexPath.row]
        
        cell!.viewModel = beaconCellViewModel
        cell!.viewModel.callback = self
        
        cell!.batteryLevelLabel.text = String(beaconCellViewModel.batteryLevel) + " %"
        cell!.beaconName.text = beaconCellViewModel.nickname
        cell!.distanceLabel.text = String(format: "%.2f", beaconCellViewModel.distance) + " m"
        
        return cell!
    }
    
}
