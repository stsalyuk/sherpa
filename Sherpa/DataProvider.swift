//
//  DataProvider.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/23/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


protocol DataChangedCallback {
    
    func dataChanged()
    
}

internal class DataProvider {
    
    internal var callbacks: [DataChangedCallback] = []
    
    internal func onDataChanged() {
        
        for callback in callbacks {
            callback.dataChanged()
        }
    }
    
    internal func addListener(listener: DataChangedCallback) {
        callbacks.append(listener)
    }
    
}
