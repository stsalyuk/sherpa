//
//  SpinnerView.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/27/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import UIKit


enum SpinnerState : Int {
    
    case spinner
    case checkmark
}

internal class SpinnerView: UIView {
    
    internal let circleLayer: CAShapeLayer = CAShapeLayer()
    
    internal var mode: SpinnerState = .spinner {
        
        didSet {
            
            if mode == .spinner {
                setupSpinner()
                animateCircle(duration: 4.0)
            }
            else {
                setupCheckmark()
                animateCheckmark(duration: 1.0)
            }
        }
    }
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        
        //setupSpinner()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate func setupSpinner() {
        
        // Use UIBezierPath as an easy way to create the CGPath for the layer.
        // The path should be the entire circle.
        let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width - 10)/2, startAngle: 0.0, endAngle: CGFloat(M_PI * 2.0), clockwise: true)
        
        // Setup the CAShapeLayer with the path, colors, and line width
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.strokeColor = UIColor.white.cgColor
        circleLayer.lineWidth = 1.0
        
        // Don't draw the circle initially
        circleLayer.strokeEnd = 0.0
        
        // Add the circleLayer to the view's layer's sublayers
        layer.addSublayer(circleLayer)
    }
    
    fileprivate func setupCheckmark() {
        
        let width = frame.size.width * 0.8
        let offset = (frame.size.width - width) / 2.0
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: offset + width * 0.15, y: offset + width * 0.5))
        path.addLine(to: CGPoint(x: offset + width * 0.37, y: offset + width * 0.75))
        path.addLine(to: CGPoint(x: offset + width * 0.87, y: offset + width * 0.25))
        
        circleLayer.path = path.cgPath
        circleLayer.strokeEnd = 0.0
    }
    
    fileprivate func animateCircle(duration: TimeInterval) {
        
        circleLayer.removeAllAnimations()
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        animation.duration = duration
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        animation.repeatCount = Float(Int.max)
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCircle")
    }
    
    fileprivate func animateCheckmark(duration: TimeInterval) {
        
        circleLayer.removeAllAnimations()
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        
        animation.duration = duration
        // Animate from 0 (no circle) to 1 (full circle)
        animation.fromValue = 0
        animation.toValue = 1
        
        // Do a linear animation (i.e. the speed of the animation stays the same)
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
        
        // Set the circleLayer's strokeEnd property to 1.0 now so that it's the
        // right value when the animation ends.
        circleLayer.strokeEnd = 1.0
        
        // Do the actual animation
        circleLayer.add(animation, forKey: "animateCheckmark")
    }
    
    
    
    override func draw(_ rect: CGRect) {
        
    }
}
