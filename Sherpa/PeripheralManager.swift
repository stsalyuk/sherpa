//
//  PeripheralManager.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import CoreBluetooth

internal class PeripheralManager: NSObject, CBPeripheralDelegate {
    
    internal var peripheral: CBPeripheral!
    internal var callback: PeripheralCallback?
    internal var isConnected = false
    
    internal var peripheralData: [String: AnyObject?] = [:]
    
    internal var beaconId: String!
    
    internal var rssi = 0.0
    internal var batteryLevel = 0
    internal var manufacturerName = ""
    internal var modelNumber = ""
    
    internal var batteryService: CBService?
    internal var batteryCharacteristic: CBCharacteristic?
    
    internal var didTimeoutConnecting = false
    
    
    // MARK: Service 1
    
    internal static let service1 = CBUUID(string: "0000F101-AA55-1001-0058-69616E674D61")
    // [read notify]
    internal static let characteristics1Service1 = CBUUID(string: "0000F102-AA55-1001-0058-69616E674D61")
    // [read, writeWithoutResponse, write]
    internal static let characteristics2Service1 = CBUUID(string: "0000F103-AA55-1001-0058-69616E674D61")
    // [read]
    internal static let characteristics3Service1 = CBUUID(string: "0000F104-AA55-1001-0058-69616E674D61")
    
    
    // MARK: Battery Service
    
    fileprivate let batteryServiceUUID = CBUUID(string: "180F")
    fileprivate let batteryCharacteristicUUID = CBUUID(string: "2A19")
    
    
    // MARK: Device Info
    
    fileprivate let deviceInformationService = CBUUID(string: "180A")
    fileprivate let manufacturerNameUUID = CBUUID(string: "2A29")
    fileprivate let modelNumberUUID = CBUUID(string: "2A24")
    
    
    // MARK: Service 4
    
    fileprivate let service4 = CBUUID(string: "00001530-1212-EFDE-1523-785FEABCD123")
    fileprivate let characteristic1Service4 = CBUUID(string: "00001532-1212-EFDE-1523-785FEABCD123")
    fileprivate let characteristic2Service4 = CBUUID(string: "00001531-1212-EFDE-1523-785FEABCD123")
    fileprivate let characteristic3Service4 = CBUUID(string: "00001534-1212-EFDE-1523-785FEABCD123")
    
    init(_ advertisement: AdvertisementInfo) {
        
        super.init()
        
        self.peripheral = advertisement.peripheral
        self.peripheral.delegate = self
        
        beaconId = advertisement.beaconId
    }
    
    internal func discoverServices() {
        
        print("discovering services")
        
        peripheral.discoverServices([batteryServiceUUID])
        
        //Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.readRSSI), userInfo: nil, repeats: true)
    }
    
    internal func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
    
        rssi = RSSI.doubleValue
    }
    
    dynamic func readRSSI() {
        
        peripheral.readRSSI()
    }
    
    internal func readBatteryLevel() {
        
        if let characteristic = batteryCharacteristic {
            
            peripheral.readValue(for: characteristic)
        }
    }
}


/**
 Update Callbacks
*/
extension PeripheralManager {
    
    internal func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        
        if let data = characteristic.value {
            
            // The results are in Little Endian!!!
            
            if characteristic.uuid.uuidString == batteryCharacteristicUUID.uuidString {
                
                batteryLevel = Int(data.toByteArray()[0])
                
                print("battery level :\(batteryLevel) %")
                
                callback?.didUpdateBatteryLevel(batteryLevel, peripheral: self)
                
                //peripheralData[batteryCharacteristicUUID.uuidString] = Int(data.toByteArray()[0]) as AnyObject??
            }
            else if characteristic.uuid.uuidString == manufacturerNameUUID.uuidString {
                
                if let stringValue = String(data: data) {
                    
                    manufacturerName = stringValue
                    //peripheralData[manufacturerNameUUID.uuidString] = stringValue as AnyObject??
                }
            }
            else if characteristic.uuid.uuidString == modelNumberUUID.uuidString {
                if let stringValue = String(data: data) {
                    
                    modelNumber = stringValue
                    //peripheralData[modelNumberUUID.uuidString] = stringValue as AnyObject??
                }
            }
            
            // Check to see if all the data we are expecting has been received.
//            var didReadAllData = true
//            for (_, v) in peripheralData {
//                
//                if v == nil {
//                    didReadAllData = false
//                    break
//                }
//            }
//            
//            if didReadAllData {
//                
//                callback?.didReadAllData()
//            }
        }
    }
    
    fileprivate func printData(_ data: Data, characteristic: CBCharacteristic) {
        
        print("byteString = \(data.toByteArrayString())")
        print("byte16 = \(data.toByteArrayString16())")
        
        if let value = Int(data: data) {
            print("characteristic: \(characteristic.description) = \(value)")
        }
        else if let value = Double(data: data) {
            print("characteristic: \(characteristic.description) = \(value)")
        }
        else if let value = String(data: data) {
            print("characteristic: \(characteristic.description) = \(value)")
        }
    }
    
}


/**
 Discovery callbacks
 */
extension PeripheralManager {

    internal func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        print("discovered services")
        
        if let services = self.peripheral.services {
            
            for service in services {
                
                if service.uuid == batteryServiceUUID {
                    
                    print("discovered battery service")
                    
                    batteryService = service
                    peripheral.discoverCharacteristics([batteryCharacteristicUUID], for: batteryService!)
                    break
                }
            }
        }
    }
    
    internal func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if let characteristics = service.characteristics {
            
            for characteristic in characteristics {
                
                
//                if characteristic.uuid.uuidString == modelNumberUUID.uuidString {
//                    peripheralData[modelNumberUUID.uuidString] = nil
//                }
//                if characteristic.uuid.uuidString == manufacturerNameUUID.uuidString {
//                    peripheralData[manufacturerNameUUID.uuidString] = nil
//                }
                if characteristic.uuid.uuidString == batteryCharacteristicUUID.uuidString {
                    
                    print("discovered battery characteristic")
                    
                    batteryCharacteristic = characteristic
                    peripheral.readValue(for: batteryCharacteristic!)
                    break
                }
                
//                if characteristic.properties.contains(.read) {
//                    self.peripheral.readValue(for: characteristic)
//                }
                
//                if characteristic.properties.contains(.notify) {
//                    self.peripheral.setNotifyValue(true, for: characteristic)
//                }
            }
        }
    }
}
