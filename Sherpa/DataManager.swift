//
//  DataManager.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


internal class DataManager : DataProvider {
    
    internal static let shared = DataManager()
    
    
    
    internal fileprivate(set) var advertisementHistory = AdvertisementHistory()
    
    internal var lastAdvertisement: AdvertisementInfo? {
        get {
            return advertisementHistory.latestAdvertisement
        }
    }
    
    internal var numAdsReceived: Int64 = 0
    
    private override init() {
        
        
    }
    
    internal func newAdvertisement(_ advertisement: AdvertisementInfo) {
        
        //BatteryMonitor.shared.startMonitoring(advertisement)
        
        // If the distance is greater than max distance threshold this is an outlier.
        guard advertisement.distance < Utilities.maxDistanceInMeters else {
            return
        }

        advertisement.isOutlier = advertisementHistory.isOutlier(advertisement)
        advertisementHistory.add(advertisement)
        numAdsReceived += 1
        
        onDataChanged()
    }
    
}
