//
//  AdvertisementInfo.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import CoreBluetooth


internal class AdvertisementInfo {
    
    /// kCBAdvDataLocalName: (sample: Allile_VI16)
    internal var name = ""

    /// kCBAdvDataManufacturerData: (sample: aa553a03 d431fe33 8af8)
    internal var manufacturerData = "" {
        
        didSet {
            
            let end = manufacturerData.index(manufacturerData.endIndex, offsetBy: -6)
            beaconId = manufacturerData.substring(from: end)
        }
    }
    
    ///kCBAdvDataTxPowerLevel (in dBm). More negative is weaker. Less negative is stronger.
    internal var txPower = 0.0
    
    internal var rssi: Double = 0.0
    
    internal var timestamp = Date()
    
    internal var beaconId = ""
    
    internal var isOutlier = false
    
    internal var peripheral: CBPeripheral!
    
    // don't think i need this.
    //internal var nickname = ""
    
    internal var distance = 0.0
    
    init(_ name: String, data: String, power: Double, rssi: Double, peripheral: CBPeripheral) {
        
        self.name = name
        self.manufacturerData = data
        self.txPower = power
        self.rssi = rssi
        
        let end = manufacturerData.index(manufacturerData.endIndex, offsetBy: -6)
        beaconId = manufacturerData.substring(from: end)
        self.peripheral = peripheral
    }
    
}


internal class AdvertisementHistory {
    
    private var list: [AdvertisementInfo] = []
    
    private var capacity = AppSettings.shared.cacheCapacity
    
    internal var latestAdvertisement: AdvertisementInfo? {
        
        get {
            return (list.count == 0) ? nil : list[list.count - 1]
        }
    }
    
    // List of ids of unique devices we've encountered during runtime.
    internal var discoveredBeaconList: [String] = []
    
    init() {
        
    }
    
    convenience init(_ capacity: Int) {
        
        self.init()
        self.capacity = capacity
    }
    
    internal func add(_ advertisement: AdvertisementInfo) {
        
        if list.count < capacity {
            
            list.append(advertisement)
        }
        else {
            
            list.removeFirst()
            list.append(advertisement)
        }
        
        if !discoveredBeaconList.contains( where: { $0 == advertisement.beaconId }) {
            discoveredBeaconList.append(advertisement.beaconId)
        }
    }
    
    internal func getLast(count: Int, byId id: String) -> [AdvertisementInfo] {
        
        var advertisements: [AdvertisementInfo] = []
        
        advertisements = list.filter({$0.beaconId == id})
        
        if advertisements.count > count {
            
            advertisements.removeFirst(advertisements.count - count)
        }
        
        return advertisements
    }
    
    internal func getAll(_ id: String) -> [AdvertisementInfo] {
        
        var advertisements: [AdvertisementInfo] = []
        
        advertisements = list.filter({$0.beaconId == id})
        
        return advertisements
    }
    
    
    internal static func getBestFitLineSlope(_ data: [AdvertisementInfo]) -> Double {
        
        let xValues: [Double] = data.map({ return Double($0.timestamp.timeIntervalSinceReferenceDate) })
        let yValues: [Double] = data.map({ return $0.distance })
        
        return Utilities.getBestFitLineSlope(xValues: xValues, yValues: yValues)
    }
    
    internal func getAverageDistance(_ beaconId: String) -> Double {
        
        let recentDistances = getLast(count: AppSettings.shared.filterWindowSize, byId: beaconId).map({ return $0.distance })
        let average = recentDistances.reduce(0, +) / Double(recentDistances.count)
        
        return average
    }
    
    internal func existsInCache(_ beaconId: String) -> Bool {
        
        if list.contains(where: {$0.beaconId == beaconId }) {
            return true
        }
        
        return false
    }
    
    /*
     
     Dmax ≥ (ABS(x − μ))/σ
     
     Dmax = maximum allowable deviation
     μ = mean
     σ = standard deviation
     x = distance value
     
    */
    internal func isOutlier(_ advertisement: AdvertisementInfo) -> Bool {
        
        var isOutlier = false
        
        let matchingAds = list.filter({$0.beaconId == advertisement.beaconId })
        
        // If the number of ads recv'd from this beacon is less than the filter window, 
        // there is not enough data to compute, so this can't be an outlier
        guard matchingAds.count >= AppSettings.shared.filterWindowSize else {
            return false
        }
        
        let distanceValues: [Double] = matchingAds.map({ return $0.distance })
        
        let mean = getAverageDistance(advertisement.beaconId)
        let standardDeviation = Utilities.computeStandardDeviation(distanceValues)
        
        // If a distance value is more than 2 times the standardDeviation, it is an outlier
        let outlierThreshold = 3.0
        
        //Dmax ≥ (ABS(x − μ))/σ
        let criterion = abs(advertisement.distance - mean) / standardDeviation
        
        if criterion > outlierThreshold {
            isOutlier = true
        }
                
        return isOutlier
    }
    
    
}
