//
//  ViewModelCallback.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/25/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


protocol ViewModelCallback {
    
    
    func viewModelUpdated()
}
