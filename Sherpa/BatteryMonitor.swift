//
//  BatteryMonitor.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/19/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import CoreBluetooth


internal class BatteryData {
    
    internal var lastReadTime: Date?
    internal var beaconId = ""
    internal var batteryLevel = 0
    internal var peripheral: PeripheralManager
    
    init(_ peripheral: PeripheralManager) {
        
        self.peripheral = peripheral
        beaconId = peripheral.beaconId
    }
}


internal class BatteryMonitor: DataProvider, PeripheralCallback, ConnectionCallback {
    
    internal static let shared = BatteryMonitor()
    
    internal var batteryData: [BatteryData] = []
    
    private override init() {
        
        super.init()
        
        BluetoothManager.shared.callback = self
    }
    
    
    func startMonitoring(_ advertisement: AdvertisementInfo) {
        
        if !batteryData.contains(where: {$0.beaconId == advertisement.beaconId}) {
            
            BluetoothManager.shared.connect(PeripheralManager(advertisement))
        }
    }
    
    func didUpdateBatteryLevel(_ batteryLevel: Int, peripheral: PeripheralManager) {
        
        if let data = batteryData.filter({$0.beaconId == peripheral.beaconId}).first {
            
            data.lastReadTime = Date()
            data.batteryLevel = batteryLevel
        }
        
        print("\(BeaconRegistrar.getNickname(peripheral.beaconId)) battery level: \(batteryLevel)")
        
        BluetoothManager.shared.disconnect(peripheral)
        
        onDataChanged()
    }
    
    
    func peripheralConnected(_ peripheralManager: PeripheralManager) {
        
        if !batteryData.contains(where: {$0.beaconId == peripheralManager.beaconId}) {
            
            let data = BatteryData(peripheralManager)
            batteryData.append(data)
        }
        
        print("connected to \(BeaconRegistrar.getNickname(peripheralManager.beaconId))")
        
        peripheralManager.discoverServices()
        peripheralManager.isConnected = true
        peripheralManager.callback = self
    }
    
    func peripheralDisconnected(_ peripheralManager: PeripheralManager) {
        
        print("disconnected from \(BeaconRegistrar.getNickname(peripheralManager.beaconId))")
        
        for (index, data) in batteryData.enumerated() {
            
            if data.beaconId == peripheralManager.beaconId {
                batteryData.remove(at: index)
                break
            }
        }
    }
    
    /**
     Called when a peripheral times out while reading the battery
    */
    func peripheralTimedOut(_ peripheralManager: PeripheralManager) {
        
//        if peripheralManager.didTimeoutConnecting {
//            
//            Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.reconnect(sender:)), userInfo: ["peripheral" : peripheralManager], repeats: false)
//        }
    }
    
    /**
     Reconnects to a peripheral after the connection has timed out.
    */
    @objc func reconnect(sender: Timer) {
        
        if let userInfo = sender.userInfo as? [String: PeripheralManager] {
        
            if let peripheral = userInfo["peripheral"] {
                
                print("attempting to reconnect to \(BeaconRegistrar.getNickname(peripheral.beaconId))")
                BluetoothManager.shared.connect(peripheral)
            }
        }
    }
}
