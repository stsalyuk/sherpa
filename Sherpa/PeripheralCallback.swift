//
//  PeripheralCallback.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/21/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation


protocol PeripheralCallback {
    
    func didUpdateBatteryLevel(_ batteryLevel: Int, peripheral: PeripheralManager)
}
