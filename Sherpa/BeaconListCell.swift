//
//  BeaconListCell.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 5/18/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import UIKit
import Charts

internal class BeaconListCell: UITableViewCell {
    
    
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var beaconName: UILabel!
    @IBOutlet weak var batteryLevelLabel: UILabel!
    @IBOutlet weak var chartView: UIView!
    
    var viewModel: BeaconCellViewModel!
    
    var chart: LineChartView!
    var dataSet: LineChartDataSet!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        chart = LineChartView(frame: CGRect(x: 0, y: 0, width: chartView.frame.width, height: chartView.frame.height))
        chart.backgroundColor = NSUIColor.clear
        chart.leftAxis.axisMinimum = 0.0
        chart.rightAxis.axisMinimum = 0.0
        chart.chartDescription?.text = nil
        chart.xAxis.gridColor = .clear
        chartView.addSubview(chart)
    }
    
    func reloadChart() {
        
        chart.data = LineChartData(dataSet: viewModel.chartDataSet)
    }
}
