//
//  SpeechManager.swift
//  Sherpa
//
//  Created by Serge Tsalyuk on 1/28/17.
//  Copyright © 2017 Serge Tsalyuk. All rights reserved.
//

import Foundation
import AudioToolbox
import AVFoundation


internal class SpeechManager {
    
    internal static let shared = SpeechManager()
    
    private let synthesizer = AVSpeechSynthesizer()
    
    private var speakQueue: [String: Date] = [:]
    
    
    private init() {
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.interruptSpokenAudioAndMixWithOthers)
            try AVAudioSession.sharedInstance().setMode(AVAudioSessionModeSpokenAudio)
            try AVAudioSession.sharedInstance().setActive(true, with: AVAudioSessionSetActiveOptions.notifyOthersOnDeactivation)
            
        } catch {
            print(error)
        }

    }
    
    internal func speak(_ text: String) {
        
        var canSpeak = true
        for (k, v) in speakQueue {
            
            if k == text {
                let lastPlayTime = v
                if abs(lastPlayTime.timeIntervalSinceNow) < 15.0 {
                    
                    canSpeak = false
                }
            }
        }
        
        if canSpeak {
        
            let utterance = AVSpeechUtterance(string: text)
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            
            synthesizer.speak(utterance)
            
            speakQueue[text] = Date()
        }
        
        
    }
    
}
