//
//  SherpaTests.swift
//  SherpaTests
//
//  Created by Serge Tsalyuk on 12/30/16.
//  Copyright © 2016 Serge Tsalyuk. All rights reserved.
//

import XCTest
@testable import Sherpa

class SherpaTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBestFitLineSlope() {
        
        let xValues: [Double] = [8, 2, 11, 6, 5, 4, 12, 9, 6, 1]
        let yValues: [Double] = [3, 10, 3, 6, 8, 12, 1, 4, 9, 14]
        
        let slope = Utilities.getBestFitLineSlope(xValues: xValues, yValues: yValues)
        
        XCTAssertEqual(slope, (-131.0 / 118.4))
    }
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
